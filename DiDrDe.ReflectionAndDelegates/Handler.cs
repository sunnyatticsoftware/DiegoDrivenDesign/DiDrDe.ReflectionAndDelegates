﻿using System;
using System.Threading.Tasks;

namespace DiDrDe.ReflectionAndDelegates
{
    public class Handler
    {
        public async Task Handle(object obj, Func<Task> onSuccess)
        {
            await Console.Out.WriteLineAsync($"Handling {obj}");
            await Console.Out.WriteLineAsync($"Now trying to invoke delegate asynchronously...");
            await onSuccess.Invoke();
        }
    }
}
